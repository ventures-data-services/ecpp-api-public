# ECPP API Share

Some of the parts of the structure are optional (such as a basic web visualization), if you don't require some of the features from this template please refer to the [customization](#customization) section. Further notes about the structure of this project can be found in the [Technical Notes](#technical-notes) section.

## Set Up

1. Upload the *ecpp-code.zip* to S3, either by creating a new bucket or placing it in an existing one
2. Go to Cloudformation and create a stack with new resources, uploading the *ecpp-template.yaml* file as the template (**make sure to create the Cloudformation stack in the Sydney region**)
3. Input the stack parameters using the following guide:

| **Name**            | **Description**                                              | **Example**            |
| ------------------- | ------------------------------------------------------------ | ---------------------- |
| Prefix Name         | The prefix used in naming all of the stack's resources, recommended single alpha numeric word, probably what environment the stack is deployed in. | prod                   |
| Code Bucket         | The S3 bucket where the code zip is located                  | code-s3                |
| Code Key            | The key for the code zip inside the code bucket              | prefix/ecpp-code.zip   |
| User Names          | Two user names in a comma delimited list                     | user-1, user-2         |
| User Init Passwords | Two user passwords in a comma delimited list, users will be required to change their password on first login | p4ssw0rd-1, p4ssw0rd-2 |

4. Go to API Gateway in the AWS console and find the ID for the newly created API
5. In the index.js file copy and paste your API ID into the URL line under the comment PUT API URL BELOW (there are two of these). For example:

```js
// PUT API URL BELOW
url: 'https://YOUR-ID.execute-api.ap-southeast-2.amazonaws.com/Prod/data'
```

6. Move the index.js file into the visuals bucket

Once you have data sent through the API you may need to run the Data Crawler in the AWS Glue Crawler to create the table. This has already been set up to run automatically once per day, but may not have run yet when you have first built the stack. **NOTE: you must run the data crawler over the data at least once before the Athena queries or visualizations will work**.

## Customization

Customization can be done before stack creation or by updating your already created stack at any time.

### Remove Athena Users

To remove the Athena users remove or comment out the following resources from the Cloudformation template:

- User1
- User2
- User1AccessKey
- User2AccessKey
- EcppAthenaWorkGroup
- ProgramaticGroup

And the parameters:

- UserNames
- UserInitPasswords

As well as all the Outputs.

After this is completed update your Cloudformation stack with your new template file.

### Remove Web Visualizations

To remove the web visualization front end (and the GET from the API) remove or comment out the following resources from the Cloudformation template:

- PUBLICwebVisualBucket
- VisualAnalyticsLambda
- DataSetApiResource
- ProxyDataSetApiResource
- DataSetRequestGet
- DataSetRequestOptions

After this is completed update your Cloudformation stack with your new template file.

## Technical Notes

The ECPP API Share is made up of four parts:

1. [API for data input](#input-api)
2. [Mini data lake for data storage](#storage-data-lake)
3. [IAM users with Athena access for data usage](#usage-athena)
4. [Web visuals](#web-visuals)

Each part is contained within the ECPP Cloudformation YAML template.

### Input  API

Data input is done through an AWS API Gateway through POST events on event type resources. This triggers the API Responder Lambda function which places the data provided in the POST request in raw form into the Raw Data S3 Bucket, and a modified version of the data in the Transformed Data S3 Bucket.

### Storage Data Lake

There is a Glue Data Crawler that is scheduled to run ONCE a day.

Data storage is handled by a light weight data lake model. It contains two data S3 buckets for Raw and Transformed data, a Glue Crawler and Database for the Transformed Data Bucket.

No ETL is needed as any transformation is done via the API Lambda function when it saves the data to the Transformed Bucket. 

### Usage Athena

Usage of data is set via IAM Users that have programmatic access (*no* console access) to an Athena Workgroup (created by the template), Glue Transformed Database, and the Transformed S3 Objects in the Transformed Bucket.

Results from the Athena queries are stored in the Results Bucket also created by the template.

### Web Visuals

We supply a basic visuals web page which summarizes some of the data by using the GET method through the API. This is placed in a publicly read accessible bucket so the link can be shared and viewed without giving direct access to the data.

Use of this webpage triggers a lambda which uses a GET request on the API to build the visualizations.