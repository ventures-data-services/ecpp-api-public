# Ventures
# 11/05/2019

AWSTemplateFormatVersion: 2010-09-09
Description: ECPP API and Athena data share
Resources:

  # S3 #

  RawDataBucket:
    Type: 'AWS::S3::Bucket'
    Properties:
      BucketName: !Join 
        - ''
        - - !Ref PrefixName
          - -ecpp-raw-s3
      BucketEncryption:
        ServerSideEncryptionConfiguration:
          - ServerSideEncryptionByDefault:
              SSEAlgorithm: AES256
      PublicAccessBlockConfiguration:
        BlockPublicAcls: true
        IgnorePublicAcls: true
        BlockPublicPolicy: true
        RestrictPublicBuckets: true
      VersioningConfiguration: 
        Status: Enabled
      Tags:
        - Key: Purpose
          Value: ecpp Lake Bucket
        - Key: Role
          Value: Storing ecpp data files

  TransformedDataBucket:
    Type: 'AWS::S3::Bucket'
    Properties:
      BucketName: !Join 
        - ''
        - - !Ref PrefixName
          - -ecpp-transformed-s3
      BucketEncryption:
        ServerSideEncryptionConfiguration:
          - ServerSideEncryptionByDefault:
              SSEAlgorithm: AES256
      PublicAccessBlockConfiguration:
        BlockPublicAcls: true
        IgnorePublicAcls: true
        BlockPublicPolicy: true
        RestrictPublicBuckets: true
      VersioningConfiguration: 
        Status: Enabled
      Tags:
        - Key: Purpose
          Value: ecpp Lake Bucket
        - Key: Role
          Value: Storing ecpp data files

  QueryBucket:
    Type: 'AWS::S3::Bucket'
    Properties:
      BucketName: !Join 
        - ''
        - - !Ref PrefixName
          - -ecpp-query-s3
      BucketEncryption:
        ServerSideEncryptionConfiguration:
          - ServerSideEncryptionByDefault:
              SSEAlgorithm: AES256
      PublicAccessBlockConfiguration:
        BlockPublicAcls: true
        IgnorePublicAcls: true
        BlockPublicPolicy: true
        RestrictPublicBuckets: true
      VersioningConfiguration: 
        Status: Enabled
      Tags:
        - Key: Purpose
          Value: ecpp Lake Bucket
        - Key: Role
          Value: Storing ecpp API query results

  # THIS BUCKET WILL HAVE PUBLIC READ ACCESS #
  PUBLICwebVisualBucket:
    Type: 'AWS::S3::Bucket'
    Properties:
      BucketName: !Join 
        - ''
        - - !Ref PrefixName
          - -ecpp-visuals-s3
      PublicAccessBlockConfiguration:
        BlockPublicAcls: false
        IgnorePublicAcls: false
        BlockPublicPolicy: false
        RestrictPublicBuckets: false
      VersioningConfiguration: 
        Status: Enabled
      AccessControl: PublicRead
      Tags:
        - Key: Purpose
          Value: ecpp web front end
        - Key: Role
          Value: web front end index bucket

  # GLUE #

  ecppDatabase:
    Type: 'AWS::Glue::Database'
    Properties:
      CatalogId: !Ref 'AWS::AccountId'
      DatabaseInput:
        Name: !Join 
          - ''
          - - !Ref PrefixName
            - _ecpp_glu
        Description: Stores tables for the ecpp data

  ecppDataCrawler:
    Type: 'AWS::Glue::Crawler'
    Properties:
      Name: !Join 
        - ''
        - - !Ref PrefixName
          - -ecpp-crl
      Role: !GetAtt 
        - ecppDataCrawlerRole
        - Arn
      TablePrefix: ''
      Schedule:
        ScheduleExpression: cron(15 20 * * ? *)
      Description: AWS Glue crawler to crawl the ecpp Data Bucket
      DatabaseName: !Ref ecppDatabase
      Targets:
        S3Targets:
          - Path: !Ref TransformedDataBucket
      SchemaChangePolicy:
        UpdateBehavior: UPDATE_IN_DATABASE
        DeleteBehavior: DELETE_FROM_DATABASE
      Configuration: >-
        {"Version":1.0,"CrawlerOutput":{"Partitions":{"AddOrUpdateBehavior":"InheritFromTable"},"Tables":{"AddOrUpdateBehavior":"MergeNewColumns"}}}
    DependsOn: TransformedDataBucket


  # LAMBDA #

  ApiResponderLambda: 
    Type: "AWS::Lambda::Function"
    Properties: 
      Handler: "ecpp-api-lambda.lambda_handler"
      Role: 
        Fn::GetAtt: 
          - LambdaRole
          - "Arn"
      Code:
        S3Bucket: !Ref CodeBucket
        S3Key: !Ref  CodeKey
      Runtime: "python3.8"
      Timeout: 600
      Environment:
        Variables:
          TransformedDataBucket: !Ref TransformedDataBucket
      Tags:
        -
          Key: "Purpose"
          Value: "Lambda Function"
        -
          Key: "Role"
          Value: "Saves put requests"

  VisualAnalyticsLambda: 
    Type: "AWS::Lambda::Function"
    Properties: 
      Handler: "ecpp-analytics.handler"
      Role: 
        Fn::GetAtt: 
          - LambdaRole
          - "Arn"
      Code:
        S3Bucket: !Ref CodeBucket
        S3Key: !Ref  CodeKey
      Runtime: "nodejs12.x"
      Timeout: 600
      Environment:
        Variables:
          db_name: !Ref ecppDatabase
          # data is stored at root of bucket, so table name will be bucket name
          table_name: !Ref TransformedDataBucket
          s3_location: !Sub s3://${QueryBucket}/
      Tags:
        -
          Key: "Purpose"
          Value: "Lambda Function"
        -
          Key: "Role"
          Value: "Saves put requests"

  # API GATEWAY #

  apiGateway:
    Type: "AWS::ApiGateway::RestApi"
    Properties:
      Name: !Sub ${PrefixName}-ecpp-api
      Description: "API for ecpp"
      ApiKeySourceType: HEADER
      EndpointConfiguration: 
        Types: 
          - REGIONAL

  lambdaApiGatewayInvoke:
    Type: "AWS::Lambda::Permission"
    Properties:
      Action: "lambda:InvokeFunction"
      FunctionName: !GetAtt "ApiResponderLambda.Arn"
      Principal: "apigateway.amazonaws.com"
      SourceArn: !Sub "arn:aws:execute-api:${AWS::Region}:${AWS::AccountId}:${apiGateway}/*/POST/event"

  lambdaApiGatewayInvokeGet:
    Type: "AWS::Lambda::Permission"
    Properties:
      Action: "lambda:InvokeFunction"
      FunctionName: !GetAtt "VisualAnalyticsLambda.Arn"
      Principal: "apigateway.amazonaws.com"
      SourceArn: !Sub "arn:aws:execute-api:${AWS::Region}:${AWS::AccountId}:${apiGateway}/*/GET/dataset/*"

  ProdApiStage:
    DependsOn:
    - ApiGatewayAccount
    Type: AWS::ApiGateway::Stage
    Properties:
      DeploymentId:
        Ref: ApiDeployment
      MethodSettings:
      - DataTraceEnabled: true
        HttpMethod: "*"
        LoggingLevel: INFO
        ResourcePath: "/*"
        MetricsEnabled: true
      RestApiId:
        Ref: apiGateway
      StageName: Prod
      Variables:
        raw_bucket: !Ref RawDataBucket
        trans_bucket: !Ref TransformedDataBucket

  ApiDeployment:
    Type: AWS::ApiGateway::Deployment
    DependsOn:
    - EventRequestPost
    - DataSetApiResource
    Properties:
      RestApiId:
        Ref: apiGateway
      StageName: DummyStage

  # Event resources

  EventApiResource:
    Type: AWS::ApiGateway::Resource
    Properties:
      RestApiId:
        Ref: apiGateway
      ParentId:
        Fn::GetAtt:
        - apiGateway
        - RootResourceId
      PathPart: event

  EventRequestPost:
    DependsOn: lambdaApiGatewayInvoke
    Type: AWS::ApiGateway::Method
    Properties:
      # ApiKeyRequired: true
      AuthorizationType: NONE
      HttpMethod: POST
      Integration:
        Type: AWS_PROXY
        IntegrationHttpMethod: ANY
        Uri:
          Fn::Join:
          - ''
          - - 'arn:aws:apigateway:'
            - Ref: AWS::Region
            - ":lambda:path/2015-03-31/functions/"
            - Fn::GetAtt:
              - ApiResponderLambda
              - Arn
            - "/invocations"
        IntegrationResponses:
        - StatusCode: 200
      ResourceId:
        Ref: EventApiResource
      RestApiId:
        Ref: apiGateway
      MethodResponses:
      - StatusCode: 200

  # Data Set Resources

  DataSetApiResource:
    Type: AWS::ApiGateway::Resource
    Properties:
      RestApiId:
        Ref: apiGateway
      ParentId:
        Fn::GetAtt:
        - apiGateway
        - RootResourceId
      PathPart: dataset

  ProxyDataSetApiResource:
    Type: 'AWS::ApiGateway::Resource'
    Properties:
      ParentId: !Ref DataSetApiResource
      RestApiId: !Ref apiGateway
      PathPart: '{proxy+}'

  DataSetRequestGet:
    DependsOn: lambdaApiGatewayInvokeGet
    Type: AWS::ApiGateway::Method
    Properties:
      # ApiKeyRequired: true
      AuthorizationType: NONE
      HttpMethod: GET
      Integration:
        Type: AWS_PROXY
        IntegrationHttpMethod: POST
        Uri:
          Fn::Join:
          - ''
          - - 'arn:aws:apigateway:'
            - Ref: AWS::Region
            - ":lambda:path/2015-03-31/functions/"
            - Fn::GetAtt:
              - VisualAnalyticsLambda
              - Arn
            - "/invocations"
        IntegrationResponses:
        - StatusCode: 200
      ResourceId:
        Ref: ProxyDataSetApiResource
      RestApiId:
        Ref: apiGateway
      MethodResponses:
      - StatusCode: 200
        ResponseParameters:
          method.response.header.Access-Control-Allow-Origin: '*'
      RequestParameters:
        method.request.path.proxy: true

  DataSetRequestOptions:
    DependsOn: lambdaApiGatewayInvokeGet
    Type: AWS::ApiGateway::Method
    Properties:
      ApiKeyRequired: true
      AuthorizationType: NONE
      HttpMethod: OPTIONS
      Integration:
        Type: MOCK
        IntegrationHttpMethod: ANY
        IntegrationResponses:
          - StatusCode: 200
            ResponseParameters:
              method.response.header.Access-Control-Allow-Headers: "'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token'"
              method.response.header.Access-Control-Allow-Methods: "'GET,DELETE,POST,OPTIONS'"
              method.response.header.Access-Control-Allow-Origin: "'*''"
      ResourceId:
        Ref: ProxyDataSetApiResource
      RestApiId:
        Ref: apiGateway
      MethodResponses:
      - StatusCode: 200
        ResponseParameters:
          method.response.header.Access-Control-Allow-Headers: "'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token'"
          method.response.header.Access-Control-Allow-Methods: "'GET,DELETE,POST,OPTIONS'"
          method.response.header.Access-Control-Allow-Origin: "'*'"
      RequestParameters:
        method.request.path.proxy: true

  # DomainNameApi:
  #   Type: 'AWS::ApiGateway::DomainName'
  #   Properties:
  #     RegionalCertificateArn: !Ref Certificate
  #     DomainName: !Ref DomainName
  #     EndpointConfiguration:
  #       Types:
  #         - REGIONAL

  # DomainMapping:
  #   Type: 'AWS::ApiGateway::BasePathMapping'
  #   Properties:
  #     DomainName: !Ref DomainNameApi
  #     RestApiId: !Ref apiGateway
  #     Stage: !Ref ProdApiStage

  # RecordSet:
  #   Type: AWS::Route53::RecordSet
  #   Properties:
  #     HostedZoneId:
  #       Ref: HostedZoneId
  #     Name:
  #       Ref: DomainName
  #     Type: A
  #     AliasTarget:
  #       HostedZoneId:
  #         Fn::GetAtt: DomainNameApi.RegionalHostedZoneId
  #       DNSName:
  #         Fn::GetAtt: DomainNameApi.RegionalDomainName

  ApiKey:
    Type: 'AWS::ApiGateway::ApiKey'
    DependsOn:
      - ApiDeployment
      - ProdApiStage
    Properties:
      Name: !Sub ${PrefixName}-ecpp-api-key
      Description: CloudFormation API Key for ecpp
      Enabled: 'true'
      StageKeys:
        - RestApiId: !Ref apiGateway
          StageName: Prod

  usagePlanKey:
    Type: 'AWS::ApiGateway::UsagePlanKey'
    Properties:
      KeyId: !Ref ApiKey
      KeyType: API_KEY
      UsagePlanId: !Ref usagePlan

  usagePlan:
    Type: 'AWS::ApiGateway::UsagePlan'
    DependsOn:
      - ApiDeployment
      - ProdApiStage
    Properties:
      ApiStages:
        - ApiId: !Ref apiGateway
          Stage: Prod
      Description: ecpp Usage Plan
      # Quota:
      #   Limit: 5000
      #   Period: DAY
      # Throttle:
      #   BurstLimit: 200
      #   RateLimit: 100
      UsagePlanName: ecpp_usage_plan

  #########
  # USERS #
  #########

  User1:
    Type: AWS::IAM::User
    Properties: 
      Groups:
        - !Ref ProgramaticGroup
      LoginProfile: 
        Password: !Select [0, !Ref UserInitPasswords]
        PasswordResetRequired: true
      UserName: !Select [0, !Ref UserNames]

  User2:
    Type: AWS::IAM::User
    Properties: 
      Groups:
        - !Ref ProgramaticGroup
      LoginProfile: 
        Password: !Select [1, !Ref UserInitPasswords]
        PasswordResetRequired: true
      UserName: !Select [1, !Ref UserNames]

  #############
  # USER KEYS #
  #############

  User1AccessKey:
    Type: AWS::IAM::AccessKey
    Properties:
      UserName:
        !Ref User1

  User2AccessKey:
    Type: AWS::IAM::AccessKey
    Properties:
      UserName:
        !Ref User2

  # ATHENA #

  EcppAthenaWorkGroup:
    Type: AWS::Athena::WorkGroup
    Properties:
      Name: !Sub ${PrefixName}-ecpp-workgroup
      Description: ECPP Athena WorkGroup
      State: ENABLED
      Tags:
        - Key: "Role"
          Value: "Athena Workgroup"
        - Key: "Purpose"
          Value: "ECPP athena workgroup"
      WorkGroupConfiguration:
        # BytesScannedCutoffPerQuery: 200000000
        EnforceWorkGroupConfiguration: true
        PublishCloudWatchMetricsEnabled: true
        RequesterPaysEnabled: false
        ResultConfiguration:
        #   EncryptionConfiguration:
        #     EncryptionOption: SSE_S3
          OutputLocation: !Sub s3://${QueryBucket}

  #######
  # IAM #
  #######

  ProgramaticGroup:
    Type: 'AWS::IAM::Group'
    Properties:
      GroupName: !Sub '${PrefixName}-wrds-programatic-${AWS::Region}'
      Policies: 
        - PolicyName: DataObjAccess-Pol
          PolicyDocument:
            Version: 2012-10-17
            Statement:
              - Effect: Allow
                Action:
                  - 's3:GetObject'
                Resource:
                  # - !Sub 'arn:aws:s3:::${TransformedDataBucket}'
                  - !Sub 'arn:aws:s3:::${TransformedDataBucket}*'
        - PolicyName: QueryBucketAccess-Pol
          PolicyDocument:
            Version: 2012-10-17
            Statement:
              - Effect: Allow
                Action:
                  - 's3:GetObject'
                  - 's3:PutObject'
                Resource:
                  - !Sub 'arn:aws:s3:::${QueryBucket}'
                  - !Sub 'arn:aws:s3:::${QueryBucket}*'
        - PolicyName: DataBucketAccess-Pol
          PolicyDocument:
            Version: 2012-10-17
            Statement:
              - Effect: Allow
                Action:
                  - 's3:ListBucket'
                Resource:
                  - !Sub arn:aws:s3:::${TransformedDataBucket}
        - PolicyName: s3ConsoleAccess-Pol
          PolicyDocument:
            Version: 2012-10-17
            Statement:
              - Effect: Allow
                Action:
                  - "s3:GetBucketLocation"
                  # - "s3:ListAllMyBuckets"
                Resource:
                  - '*'
        - PolicyName: AthenaAccess-Pol
          PolicyDocument:
            Version: 2012-10-17
            Statement:
              - Effect: Allow
                Action:
                  - "Athena:GetCatalogs"
                  - "Athena:QueryResults"
                  - "Athena:Table"
                  - "Athena:Tables"
                  - "Athena:RunQuery"
                  - "Athena:StartQueryExecution"
                  - "Athena:StopQueryExecution"
                  - "Athena:GetQueryResults"
                  - "Athena:GetQueryResultsStream"
                  - "Athena:GetQueryExecution"
                  - "Athena:GetWorkgroup"
                Resource:
                  - !Sub "arn:aws:athena:${AWS::Region}:${AWS::AccountId}:workgroup/${EcppAthenaWorkGroup}"
        - PolicyName: GlueTableAccess-Pol
          PolicyDocument:
            Version: 2012-10-17
            Statement:
              - Effect: Allow
                Action:
                  - "glue:GetTable*"
                  - "glue:GetTables"
                  - "glue:GetDatabase*"
                  - "glue:GetDatabases"
                  - "glue:GetPartition*"
                Resource:
                  - !Sub "arn:aws:glue:${AWS::Region}:${AWS::AccountId}:catalog"
                  - !Sub "arn:aws:glue:${AWS::Region}:${AWS::AccountId}:database/${ecppDatabase}"
                  - !Sub "arn:aws:glue:${AWS::Region}:${AWS::AccountId}:table/${ecppDatabase}/*"

  ApiGatewayCloudWatchLogsRole:
    Type: AWS::IAM::Role
    Properties:
      AssumeRolePolicyDocument:
        Version: '2012-10-17'
        Statement:
        - Effect: Allow
          Principal:
            Service:
            - apigateway.amazonaws.com
          Action:
          - sts:AssumeRole
      Policies:
      - PolicyName: ApiGatewayLogsPolicy
        PolicyDocument:
          Version: '2012-10-17'
          Statement:
          - Effect: Allow
            Action:
            - logs:CreateLogGroup
            - logs:CreateLogStream
            - logs:DescribeLogGroups
            - logs:DescribeLogStreams
            - logs:PutLogEvents
            - logs:GetLogEvents
            - logs:FilterLogEvents
            Resource: "*"

  ApiGatewayAccount:
    Type: AWS::ApiGateway::Account
    Properties:
      CloudWatchRoleArn:
        Fn::GetAtt:
        - ApiGatewayCloudWatchLogsRole
        - Arn

  LambdaRole:
    Type: 'AWS::IAM::Role'
    Properties:
      AssumeRolePolicyDocument:
        Version: 2012-10-17
        Statement:
          - Effect: Allow
            Principal:
              Service:
                - lambda.amazonaws.com
            Action:
              - 'sts:AssumeRole'
      Path: /
      Policies:
        - PolicyName: log-access
          PolicyDocument:
            Version: 2012-10-17
            Statement:
              - Effect: Allow
                Action:
                  - "logs:*"
                Resource:
                  - '*'
        - PolicyName: bucket-access
          PolicyDocument:
            Version: 2012-10-17
            Statement:
              - Effect: Allow
                Action:
                  - 's3:*'
                Resource:
                  - !Sub 'arn:aws:s3:::${TransformedDataBucket}*'
                  - !Sub 'arn:aws:s3:::${RawDataBucket}*'
                  # no *
                  - !Sub 'arn:aws:s3:::${TransformedDataBucket}'
                  - !Sub 'arn:aws:s3:::${RawDataBucket}'
        - PolicyName: QueryBucketAccess-Pol
          PolicyDocument:
            Version: 2012-10-17
            Statement:
              - Effect: Allow
                Action:
                  - 's3:*'
                Resource:
                  - !Sub 'arn:aws:s3:::${QueryBucket}'
                  - !Sub 'arn:aws:s3:::${QueryBucket}*'
        - PolicyName: AthenaAccess-Pol
          PolicyDocument:
            Version: 2012-10-17
            Statement:
              - Effect: Allow
                Action:
                  - "Athena:GetCatalogs"
                  - "Athena:QueryResults"
                  - "Athena:Table"
                  - "Athena:Tables"
                  - "Athena:RunQuery"
                  - "Athena:StartQueryExecution"
                  - "Athena:StopQueryExecution"
                  - "Athena:GetQueryResults"
                  - "Athena:GetQueryResultsStream"
                  - "Athena:GetQueryExecution"
                  - "Athena:GetWorkgroup"
                Resource:
                  - '*'
        - PolicyName: GlueTableAccess-Pol
          PolicyDocument:
            Version: 2012-10-17
            Statement:
              - Effect: Allow
                Action:
                  - "glue:GetTable*"
                  - "glue:GetTables"
                  - "glue:GetDatabase*"
                  - "glue:GetDatabases"
                  - "glue:GetPartition*"
                Resource:
                  - !Sub "arn:aws:glue:${AWS::Region}:${AWS::AccountId}:catalog"
                  - !Sub "arn:aws:glue:${AWS::Region}:${AWS::AccountId}:database/${ecppDatabase}"
                  - !Sub "arn:aws:glue:${AWS::Region}:${AWS::AccountId}:table/${ecppDatabase}/*"

  ecppDataCrawlerRole:
    Type: 'AWS::IAM::Role'
    Properties:
      AssumeRolePolicyDocument:
        Version: 2012-10-17
        Statement:
          - Effect: Allow
            Principal:
              Service:
                - glue.amazonaws.com
            Action:
              - 'sts:AssumeRole'
      Path: /
      ManagedPolicyArns:
        - 'arn:aws:iam::aws:policy/service-role/AWSGlueServiceRole'
      Policies:
        - PolicyName: ecpp-bucket-access
          PolicyDocument:
            Version: 2012-10-17
            Statement:
              - Effect: Allow
                Action:
                  - 's3:GetObject'
                  - 's3:PutObject'
                Resource:
                  - !Sub "arn:aws:s3:::${TransformedDataBucket}*"

###########################
# STACK INPUTS/PARAMETERS #
###########################

Metadata:
  AWS::CloudFormation::Interface:
    ParameterGroups:
      - Label:
          default: Stack Options
        Parameters:
          - PrefixName
      - Label:
          default: Code Inputs
        Parameters:
          - CodeBucket
          - CodeKey
      - Label:
          default: API Domain Inputs
        Parameters:
          - Certificate
          - DomainName
          # - HostedZoneId
      - Label:
          default: User Inputs
        Parameters:
          - UserNames
          - UserInitPasswords
    ParameterLabels:
      PrefixName:
        default: Stack Prefix
      CodeBucket:
        default: Lambda Code Bucket
      CodeKey:
        default: Lambda Code Key
      # Certificate:
      #   default: Domain Certificate ARN
      # DomainName:
      #   default: Domain Name
      # HostedZoneId:
      #   default: Domain Hosted Zone Id
      UserNames:
        default: User Names
      UserInitPasswords:
        default: User Initial Passwords


Parameters:
  PrefixName:
    Type: String
    Default: environment
    AllowedPattern: '([a-zA-Z0-9]){1}([a-zA-Z0-9-])*'
    Description: >-
      Enter a prefix that will be used on all resources in the recommended form
      <environment>
  CodeBucket:
    Type: String
    Default: code-bucket-s3
    AllowedPattern: '([a-zA-Z0-9]){1}([a-zA-Z0-9-])*'
    Description: >-
      The name of the S3 bucket where the code is stored
  CodeKey:
    Type: String
    Default: prefix/code.zip
    Description: >-
      The key of the lambda code zip file
  # Certificate:
  #   Type: String
  #   Default: arn:aws:acm:region:account:certificate/certid
  #   Description: >-
  #     The ARN of the domain certificate for API domain
  # DomainName:
  #   Type: String
  #   Default: ecpp.projects.ventures
  #   Description: >-
  #     The domain name for the API
  # HostedZoneId:
  #   Type: AWS::Route53::HostedZone::Id
  #   Description: >-
  #     The hosted zone ID for the API domain
  UserNames:
    Type: CommaDelimitedList
    Default: NewUser1, NewUser2
    Description: >-
      Enter 2 user names for the new users
  UserInitPasswords:
    Type: CommaDelimitedList
    Default: Password-1, Password-2
    Description: >-
      Enter 2 passwords for the users, they will be asked to change it on their first log in


###########
# OUTPUTS #
###########


Outputs:
  OutUser1AccessKey:
    Value:
      !Ref User1AccessKey
  OutUser1SecretKey:
    Value: !GetAtt User1AccessKey.SecretAccessKey
  OutUser2AccessKey:
    Value:
      !Ref User2AccessKey
  OutUser2SecretKey:
    Value: !GetAtt User2AccessKey.SecretAccessKey